var options = {
    desiredCapabilities: {
        browserName: 'chrome',
        chromeOptions: {
            binary: '/usr/bin/google-chrome-stable',
            args: [
                'headless',
            ],
        },
    },
}

module.exports.config = options;