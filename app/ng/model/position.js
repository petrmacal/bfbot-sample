/* Some methods and functions removed - this is just sample.... */

/*jshint esversion: 6 */
/*jshint node: true */
const EventEmitter = require('events');

const common = require('../lib/common');

/**
 * [Position] contructor
 * @param {[Object]} 	initData parsedData from Bettor
 * @param {[Function]}	dataSource function that accepts array of betIds and returns array of current orders
 */
function Position(initData, dataSource) {
	this.data = initData; // this.data.instruction.limitOrder.size for init stake
	this.counterpositions = [];
	this.counter = 0;
	this.datasource = this.dataSource(dataSource);
	this.matched = 0;
	this.debug = require('debug')('app::ng::model::position::'+initData.betId);	

	this.debug('Position %s has been added', initData.betId);

	this.attachEvents();
}

Position.prototype = Object.create(EventEmitter.prototype);

Position.prototype.attachEvents = function() {
	this.on('initialBetPlaced', this.waitForContractFullifiled.bind(this));
	this.on('contractFullifiled', this.startBacker.bind(this));
};

Position.prototype.refresh = function() {
	this.currentOrder = this.currentOrders();
	return this;
};

// Start timer of contract here
Position.prototype.waitForContractFullifiled = function() {
	setTimeout(() => this.emit('contractFullifiled'), this.data.contract * 1000);
};

// Do intital checks here after the contract has been fullifiled
Position.prototype.startBacker = function() {
	this.emit('removeInitBet', this, this.checkRemovedInitBet.bind(this));
	this.refresh().createCounterbet(this.currentOrder(this.data.betId));
};

Position.prototype.checkRemovedInitBet = function(removedTimestamp) {
	this.onDataNewerThan(removedTimestamp, function() {
		if(this.refresh().currentOrder(this.data.betId)) {
			this.createCounterbet(this.currentOrder(this.data.betId));
		} else {
			this.emit('removePosition', this, 'Nothing has been matched. Really.');
		}
	});
};

Position.prototype.createCounterbet = function(currentOrder, key = null) {
	if(!currentOrder) return;
	if (parseInt(currentOrder.sizeMatched - this.counterbetsSum()) >= 2) {
		this.counterpositions.push(1);
		this.emit('createCounterbet', this, currentOrder, key || this.counterpositions.length - 1, this.handleCounterbet.bind(this));
	}	
};

Position.prototype.handleCounterbet = function(err, res, key) {
	if(err) return this.emit('removePosition', this, 'Problem with placing counterbet :(');

	function handler(delay) {
		this.onDataNewerThan(res.timestamp + delay, function() {
			if(this.refresh().currentOrder(res.betId)) {
				if(this.currentOrder(res.betId).sizeRemaining < 2) {
					this.matched += this.currentOrder(res.betId).sizeMatched;
					this.debug('Counterposition sucessfully matched.');

					if((this.currentOrder(this.data.betId).sizeMatched - 2) <= this.matched) {
						this.emit('removePosition', this, 'All counterpositions sucessfully matched');
						this.debug('All counterpositions sucessfully matched!! Remove position now...');
					}

				} else {
					this.debug('Counterposition (%s) not matched - initial odd: %s, requested odd: %s, current odds: %o', 
						res.side,
						this.data.instruction.limitOrder.price,
						res.instruction.limitOrder.price, 
						this.datasource.actualOdds({ignoreSide:true}, res)
					);
					handler.call(this,delay+10);
				}
			} else {
				this.recreateCounterbet(key);
			}		
		});
	}

	handler.call(this,0);
};

Position.prototype.counterbetsSum = function() {
	let map 		= (bet) => bet.betId ? bet.instruction.limitOrder.size : (bet.stake ? bet.stake : 0);
	let reduction 	= (sum, curr) => sum + curr;

	return this.counterpositions.map(map).reduce(reduction,0);
};

Position.prototype.dataSource = function(source) {
	return {
		latestUpdate: () => source.getLatestOrdersUpdateTimestamp(),
		ordersData: (betIds) => source.getActualBetsByIds(betIds),
		actualOdds: (configObject = {}, position = this.data) => source.getOddsForPosition(position, configObject)
	};
};

Position.prototype.onDataNewerThan = function(timestamp, ...cbs) {
	let loop = () => {
		if(this.datasource.latestUpdate() > timestamp) {
			for(let cb of cbs) {
				cb.call(this);
			}
		} else {
			setTimeout(loop, 500);
		}
	};

	loop();
};

Position.prototype.currentOrders = function(v) {
	let reduction = (agg, curr) => agg.concat(curr); 
	let orders = common.arrayToObject(this.datasource.ordersData(new Set([String(this.data.betId), ...Object.keys(common.arrayToObject(this.counterpositions, 'betId'))].reduce(reduction,[]))), 'betId');

	//debug(orders);

	return function(betId) {
		//debug("Get %d",betId);
		if(betId) return orders[betId] ? orders[betId] : null;

		return orders;
	};
};

module.exports = Position;