/* Many methods and functions removed - this is just sample.... */

const debug = require('debug')('app::ng::ROBOT::BETTOR');
const EventEmitter = require('events');
const util = require('util');

const Ng = {};

/* Load NG components */
Ng.Events = require('./components/events');
Ng.Matches = require('./components/matches');
Ng.Positions = require('./components/positions');
Ng.Crawler = require('./marketcrawler');

/* Load model */
const Position = require('./model/position');

const inspect = (o) => console.log(util.inspect(o, {depth:null}));

function Bettor(ng, data, match) {
	this.ng = ng;
	this.matchObject = match;
	this.static = Bettor;
	this.components = Ng;
	this.data = {overunder: data, match: null};
	this.positions = new Map();
	this.history = [];
	this.lastPosition = 0;
	this.marketcrawler = null;
	this.events = [];

	this.startBettorComponents();

	if(this.data.overunder === null) {
		this.refresh();
	}
}

Bettor.config = {
	maxStake: 5, // Maximal stake for each position
	minTotalMatched: 8000
	, // Minimum total matched on particular market
	ticks: [2,1,0,-1,-2],
	contract: 30, // Seconds to contract expire
	limits: {
		minOdd: 1.2,
		maxOdd: 5
	}
}

Bettor.prototype = Object.create(EventEmitter.prototype);
Bettor.prototype.constructor = Bettor;

Bettor.prototype.startBettorComponents = function() {
	this.startRefresher();
	this.startMarketCrawler();
};

Bettor.prototype.startRefresher = function() {
	let loop = setInterval(this.refresh.bind(this),1000*60*2);

	this.events.push(loop);
};

Bettor.prototype.startMarketCrawler = function() {
	this.marketcrawler = new Ng.Crawler(this);
};

Bettor.prototype.refresh = function() {
	var self = this;

	let filter = {
		minTotalMatched: Bettor.config.minTotalMatched
	};

	//debug('Refresher running... waiting for promise to be resolved');
	this.ng.halopromise(Ng.Events.findEvent, this.matchObject.teamVariants) // Check and load match
	.then(this.ng.halo(Ng.Matches.loadMarkets, filter), function() {}) // Load and filter markets
	.then(function(res) {
		if(res) {
			// Returned over/under markets
			self.data.overunder = res.markets;
			// Returned matchOdds market
			self.data.match = res.matchOdds;
			// Returned correctScore market
			self.data.correctscore = res.correctScore;
			// Data about event (currently attribute startDate only as a fallback if no another match time source is available)
			self.matchObject.matchData.defaults = Object.assign({},res.matchData);
		}
	},this.ng.handleReject);

}

Bettor.prototype.getAllAvailableMarketIds = function() {
	return [...this.getMarketIdsFromData(), ...this.getMatchOddsMarketId(), ...this.getCorrectScoreMarketId()];
};

// Mechanism of handling signal is as follows
// 
Bettor.prototype.handleSignal = function(signal, params, conditionalBetsOnly = 0) {
	let options = Object.assign({
		contract: 35
	}, params);

	if(this.lastPosition > parseInt(Date.now()/1000) - 45) {
		//debug('--> stop <-- | %d > %d', this.lastPosition, parseInt(Date.now()/1000) - 45);
		return null;
	} else {
		this.lastPosition = parseInt(Date.now()/1000);

		let condBets = this.handleConditionalBets(options.conditionalBets);

		if(conditionalBetsOnly) {
			// If none of condition is passed, return null
			if(!condBets.passed) return null;

			debug(condBets);
			if(condBets.overunder) this.handleOverUnder(options);
			this.handleMatchOdds(options, condBets.matchodds);			
		} else {
			this.handleOverUnder(options);
			this.handleMatchOdds(options);

		}

		return 1;
	}
};

Bettor.prototype.matchHasFavorite = function() {
	let marketId = this.getMatchOddsMarketId()[0];
	//console.log("Here at matchHasFavorite");

	if(!marketId) return false;
	let runners = this.marketcrawler.getRunners(marketId);

	//console.log("Runners compare:", runners[0].lastPriceTraded, runners[1].lastPriceTraded);

	// I only need to compare runner 0 and 1 (home vs away)
	return (runners[0].lastPriceTraded / runners[1].lastPriceTraded < 0.3) || (runners[1].lastPriceTraded / runners[0].lastPriceTraded < 0.3);

}

// score parameter needs to be String in format "x:y"
Bettor.prototype.checkScoreAgainstBf = function(score) {
	let marketId = this.getCorrectScoreMarketId()[0];

	if(!marketId) return false;
	let runner = this.marketcrawler.getRunnerForMarketAndSelection(marketId, Bettor.correctScoreSelection(score));

	if(!runner) return false;

	/*
		I need to invert the truthy value. In case the odd is not presented or is more than 100 (obviously, it should be 1000 in most cases, 
		but I will rely on defensive programming, just to be sure).

		In case this method returns false, score is not probably correct. Therefore, don't bet!! Make the truthy check against this mathod
		inside all conditional bets triggered by Match instance as a first class function parameter. 
	 */

	return !!this.marketcrawler.getOddsForRunner(runner).availableToBack && !(this.marketcrawler.getOddsForRunner(runner).availableToBack > 100);
};

Bettor.prototype.handleConditionalBets = function(conditionalBets) {
	//inspect(conditionalBets);

	let result = {
		passed: false,
		overunder: false,
		matchodds: []
	};

	if(!Array.isArray(conditionalBets)) return result;	

	var parseBets = function(o) {
		return o.reduce((res, curr) => {
			if(curr.type.match(/draw|home|away/)) {
				res.matchodds.push(curr);
			} else if(curr.type === 'overunder') {
				res.overunder = true;
			}
			return res;
		}, result);
	};

	conditionalBets.forEach(function(bet) {
		// Check preconditions first
		if(!bet.preconditions) bet.preconditions = [];
		if(bet.preconditions.reduce((r, p) => r && p, true)) {
			// Now check conditions in terms of internal data or something else for actual bettor
			if(!bet.conditions) bet.conditions = [];
			if(bet.conditions.reduce((r1, p1) => {
				let res = true;
				if(p1['matchHasFavorite']) {
					res = res && this.matchHasFavorite();
				}
				if(p1['checkScore'] && p1['checkScore'] === '0:0') {
					res = res && this.checkScoreAgainstBf(p1['checkScore']);
				}

				return r1 && res;
			}, true)) {
				// Ufff.. Passed all the condiditions, so parse bets now
				parseBets(bet.bets);
				result.passed = result.passed || true;
			}
		}
	}, this);

	return result;
};


Bettor.prototype.handleOverUnder = function(options) {
	var self = this;
	var marketIds = this.getMarketIdsFromData();

	if(!marketIds.length) return;

	// The newest feature - traverse internal data directly without getting selections from BF again and again...
	// It should also contains all runners and orders made per each runner
	if(this.marketData) {

		marketIds.filter(function(market) {
			// Make sure that market is alive
			if(!self.marketcrawler.isMarketAlive(market)) {
				debug('Market %s is not alive', market);
				return;
			};

			// Get runners
			let runners = self.marketcrawler.getRunners(market);

			runners.forEach(function(runner,index) {
				if(!self.checkOdds(runner.lastPriceTraded)) {
					//debug('Odd %s does not match the requirements', runner.lastPriceTraded);
					return;
				}

				// Call processBet asynchronous
				self.ng.simpleAsync(function() {
					self.processBet(
						options,
						market,
						runner,
						index === 0 ? 'BACK' : 'LAY'
					);
				});
			});			
		});

	} else {
		// Make selection and place bet
		this.ng.halopromise(Ng.Positions.getSelections, marketIds)
			.then(this.processSelections.bind(this, options), this.ng.handleReject);
	}

};

// Process bet
Bettor.prototype.processBet = function(options, marketId, runner, side, stake = null) {
	var self = this;

	// If stake is not specified, use global maximal liability allowed for each position
	if(!stake) {
		stake = this.ng.config.bettorConfig && this.ng.config.bettorConfig.maxStake || Bettor.config.maxStake;
	}

	// Determine target odd, the side is taking into account (LAY - lower, BACK - higher)
	// Newset feature - MarketCrawler saved actual market info so check for marketData first
	let odd, _plus, initodd, tunedodd;
	if(this.marketData) {
		odd = initodd = this.marketcrawler.getOddsForRunner(runner, side, { embraceSpread: true });
		odd = tunedodd = Bettor.tuneOdd(odd, side);

		_plus = side === 'BACK' ? false : true;

		if ( (side === 'BACK' && odd < Bettor.precisionRound(Ng.Positions.oddPlusMinusTicks(runner.lastPriceTraded, 1, _plus),2)) ||
			 (side === 'LAY' && odd > Bettor.precisionRound(Ng.Positions.oddPlusMinusTicks(runner.lastPriceTraded, 1, _plus),2))
		) {
			debug('Modify %s bet odd from %s to %s and tune to %s after further checks..', side, odd, runner.lastPriceTraded, Bettor.tuneOdd(runner.lastPriceTraded, side));
			odd = Bettor.tuneOdd(runner.lastPriceTraded, side); 
		}

	} else {
		odd = Bettor.tuneOdd(runner.lastPriceTraded, side);
	}

	// If LAY order is being to be processed, let's compute exact stake in order to have the same liability as BACK side
	if(side === 'LAY') {
		stake = Ng.Positions.computeLayStake(odd, stake, Bettor.precisionRound);
	}

	if(stake < 2) {
		stake = 2;
	}

	let instruction = {
		marketId: marketId,
		selectionId: runner.selectionId,
		odd: odd,
		stake: stake,
		side: side
	};

	this.ng.halopromise(
		Ng.Positions.placeBet, 
		instruction
	).then(function(res) {
		// Check and parse placeBet method response, if success
		let betInfo = Bettor.parseBetResponse(res,marketId,runner.selectionId);

		if(betInfo) {
			// Check if bet was matched immediately and if so, assign modificator
			betInfo = Object.assign(betInfo, self.checkForImmediatelyMatchedStake(betInfo));
			betInfo.contract = options.contract; // Contact

			self.addPosition(betInfo);
		} else {
			debug('Market was not active');
		}

		debug("Bet placed (init odd: %s, tuned odd: %s, last traded odd: %s, final odd: %s): %o", initodd, tunedodd, runner.lastPriceTraded, odd, res);
	}, this.ng.handleReject);	
}


Bettor.prototype.addPosition = function(betinfo) {
	if(!betinfo) return 0;

	let position = new Position(betinfo, this.marketcrawler);
	this.positions.set(betinfo.betId, position);
	position.emit('initialBetPlaced');
	this.addPositionListeners(position);

	this.saveIntoHistory(1, betinfo);
	return 1;
};

Bettor.prototype.addPositionListeners = function(position) {
	position.on('removeInitBet', (p, cb) => this.removeInitBet(p.data.betId, p.data.marketId, cb));
	position.on('removePosition', (p, reason) => this.removePosition(p.data.betId, reason));
	position.on('createCounterbet', (p, currentOrder, key, cb) => this.processCounterBet(p, currentOrder, key, cb));
}

Bettor.prototype.removePosition = function(betId, reason) {
	debug('Removing position "%s" for ever :) Reason: %s', betId, reason);
	this.positions.get(betId).removeAllListeners();
	//this.positions.delete(betId);
}

Bettor.prototype.voidCounterposition = function(key, position, reason) {
	debug('Void counterposition with from position %s. Reason: %s', position.data, reason);
	//position.get(position).counterposition.delete(key);
	position.counterpositions[key] = -1;
}

Bettor.prototype.removeInitBet = function(betId, marketId, callback) {
	var self = this;

	debug('Remove position: (betId: %s, marketId: %s)', betId, marketId);

	if(betId != undefined && marketId != undefined) {
		// If stake is not fully matched
		//if(stake != Bettor.config.maxStake) {
			this.ng.halopromise(
				Ng.Positions.removeBet,
				{
					marketId: marketId,
					betId: betId
				}
			).then(function() {
				//debug('Removed from Betfair now..');
				callback(parseInt(Date.now() / 1000));
			}, this.ng.handleReject);
		//}
	}
}

Bettor.prototype.successfulBetOrThrottle = function(f, n = 10) {
	var i = 0, context = this;


	function run() {
		if(i++ < n) {
			f.call(context, run, n-i);
		} else {
			return;
		}
	}

	run();

};

Bettor.prototype.unmount = function() {
	this.events.map(function(e) {
		clearInterval(e);
	}, this);
};



module.exports = Bettor;