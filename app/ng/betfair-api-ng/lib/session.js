var requestBetFair = require('./request');
var _ = require('lodash');


module.exports = function (options, cb) {

  requestBetFair.login(options, function (err, token) {
    if (err) {
      return cb(err);
    }
    var session = _.extend({}, options, {token: token, lastrefresh: 0});
    cb(null, session, requestBetFair.keepalive);
  });
};
