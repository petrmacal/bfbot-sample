var Session = require('./session');
var BettingApi = require('./betting');
var AccountApi = require('./accounts');
var HeartbeatApi = require('./heartbeat');

module.exports = {
  login: login
};

function login(options, cb) {
  options = options || {};
  if (!options.username && !options.password) {
    return cb(new Error('username and password has been defined!'));
  }
  if (!options.applicationKey) {
    return cb(new Error('applicationKey should been defined!'));
  }

  Session(options, function (err, session, keepalive) {
    if (err) {
      return cb(err);
    }
    cb(null, Betfair(session, keepalive));
  });
}

function Betfair(session, keepalive) {
  return {
    session: session,
    keepalive: keepalive,
    betting: BettingApi(session),
    account: AccountApi(session),
    heartbeat: HeartbeatApi(session)
  };
}