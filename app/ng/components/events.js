const debug = require('debug')('app::ng::ROBOT::Component::Events');

Date.prototype.addHours = function(h) {
	this.setTime(this.getTime() + (h*60*60*1000));
	return this;
}

Events = {
	pairEvent: function(str,cb) {
		var self = this;
		return new Promise(function(resolve,reject) {
			self.session.then(function(bf) {
				bf.betting.listEvents({
					textQuery: str
				}, function(err,res) {
					if(res === undefined)  {
						reject('Wrong res');
						return;
					}
					let last = cb(), date = new Date();
					if(err || (res && res.length === 0) && !last) return;
					res = res.filter(function(match) {
						return new Date(match.event.openDate) < date.addHours(1) // Add 1 hour
					});

					res.length && resolve(res);
					last && reject('Not found');

					return;
				});
			});
		});
	},

	findEvent: function(teams) {
		var promises = [];

		let cb = Events.checkForNotFound(teams.size*2);

		[...teams].forEach((pair) => {
			pair = JSON.parse(pair);
			promises.push(this.halopromise(Events.pairEvent, pair.home + ' v', cb));
			promises.push(this.halopromise(Events.pairEvent, 'v ' + pair.away, cb));
		});

		return new Promise(function(resolve,reject) {
			Promise.race(promises).then(function(p,r) {
				resolve(p);
			}).catch(function(e) {
				reject([teams, e]);
			});
		});
	},

	checkForNotFound: function(n) {
		var counter = 0,
			promises = n;
		return function() {
			counter += 1;
			return counter === promises;
		}		
	}

}

module.exports = Events;