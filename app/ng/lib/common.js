module.exports = {

/**
 * [arrayToObject]
 * Convert given array of objects into object grouped by specified key.
 * @param  {Array}	array to be traversed
 * @param  {String} key is the full path to object property, eg. innerObject.betId
 * @return {Object}
 */
arrayToObject: function(array, key) {
	if (!key) {
		return false;
	}

	key = Array.isArray(key.split(/\./)) ? key.split(/\./) : [key];

	return array.reduce(function(agg, curr) {
		let val = key.reduce(function(object, key) {
			return object[key] ? object[key] : null;
		}, curr);

		if(val) agg[val] = curr;
		return agg;
	}, {});
},

/**
 * [dateToTimestamp]
 * Convert Date object, if given, into timestamp. If date is null, return actual timestamp
 * @param  {Date} date - Date object
 * @return {Number} - timestamp
 */
dateToTimestamp: function(date = null) {
	return date instanceof Date ? parseInt(date.getTime / 1000) : parseInt(Date.now() / 1000);
}

};