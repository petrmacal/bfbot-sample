const debug = require('debug')('app::ng::ROBOT');
const EventEmitter = require('events');
const mongoose = require('mongoose');
const util = require('util');

/* Load Betfair NG API */
Betfair = require('./betfair-api-ng/lib/betfair');

/* Load bettor */
Ng.Bettor = require('./bettor');

/* Load NG components */
Ng.Events = require('./components/events');
Ng.Matches = require('./components/matches');
Ng.Positions = require('./components/positions');

function Ng(config) {
	this.config = Object.assign({
		applicationKey: 'APPKEY',
		username: 'username',
		password: 'password',

		certFile: '../assets/cert/client.crt',
		keyFile: '../asets/cert/client.key'
	}, config);
	this.session = Ng.prototype.createSession.call(this);
	this.events = [];
}

Ng.prototype = Object.create(EventEmitter.prototype);
Ng.prototype.constructor = Ng;

const allowBetting = false;

Ng.createInstance = function(config) {
	let instance = new Ng(config);
	return instance;
}

Ng.prototype.createSession = function() {
	var self = this;

	let cb = function(err,res) {
		debug(res);
	};
	return new Promise(function(resolve,reject) {

		Betfair.login({
		  applicationKey: 	self.config.applicationKey, 
		  username: 		self.config.username,
		  password: 		self.config.password,
		  certFile: 		self.config.certFile,
		  keyFile: 			self.config.keyFile
		}, function(err,betfair) {
			if(err) reject(err);

			let interval = setInterval(betfair.keepalive.bind(null, betfair.session, cb), 1000*60*30);
			self.events.push(interval);
			resolve(betfair);
		});

	});
};

Ng.prototype.halopromise = function(f,...args) {
	let promise = f.apply(this, args);

	return new Promise(function(resolve,reject) {
		promise.then(function(res) {
			resolve(res);
		}).catch(function(e) {
			reject(e);
		});
	});
}

Ng.prototype.halo = function(f, ...args) {
	return f.bind(this, args);
}

Ng.prototype.handleReject = function(e) {
	debug(e);
	return false;
}

Ng.prototype.simpleAsync = function(f) {
	setTimeout(f, 0);
}

Ng.prototype.assignToInstance = function(Match) {
	var self = this;

	return new Promise(function(resolve,reject) {

		//setTimeout(() => {
			self.halopromise(Ng.Events.findEvent, Match.teamVariants) // Check and load match
			//.then(self.halo(Ng.Matches.loadMarkets),self.handleReject) // Load and filter markets
			.then(function(res) {
				if(Match.foundOnBF) return reject('Bettor already mounted');

				if(res) resolve(new Ng.Bettor(self,null,Match)); // Return new Bettor
				else reject('Wrong res (findEvent)');
			},function() {});
		//}, Math.floor(Math.random() * Math.floor(30)) * 1000);
	});
}

module.exports = Ng;
