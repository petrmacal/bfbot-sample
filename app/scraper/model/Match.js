const debug = require('debug')('app::scraper::model::Match');
const https = require('https');
const EventEmitter = require('events');
const drivers = require('../drivers');
const mongoose = require('mongoose');
const fs = require('fs');
const csvparse = require('csv-parse/lib/sync');

const helpers = require('../../lib/helpers/match.helpers');

var Match = function Match(i) {
	this.drivers = {};

	this.data = {
		teams: [i.teams.home,i.teams.away],
		created: parseInt(Date.now() / 1000),
		unique: Math.random(1000000000,9999999999),
	};

	this.teamVariants = new Set();

	this.foundOnBF = 0;

	// Actual data from drivers
	this.matchData = {
		lastUpdate: new Date(0),
		scoreActive: 0,
		score: {
			home: 0,
			away: 0
		},
		seconds: 0,
		defaults: {} // fallback (filled by betfair)
	};

	this.model = new Match.model({
		teams: i.teams,
		date: new Date(),
		object: {}
	});	

	this.history = {};

	this.ng = [];

	// Add first driver that is presented when creating instance of match
	this.addDriver({
		driver: i.driver,
		internal_id: i.internal_id
	});

	this.addTeamVariant(i.teams);
}

/** 
  * Match database schema 
*/
Match.schema = mongoose.Schema({
	teams: {
		home: String,
		away: String
	},
	date: {
		type: Date,
		default: Date.now
	},
	object: mongoose.Schema.Types.Mixed
	
});

/** 
  * Match mongoose model 
*/
Match.model = mongoose.model('Match',Match.schema);

Match.prototype = Object.create(EventEmitter.prototype);
Match.prototype.constructor = Match;

Match.prototype.addDriver = function(o) {
	if(!o.driver || !o.internal_id || !drivers[o.driver]) return 0;
 
	this.drivers[o.driver] = new drivers[o.driver](o); 
	this.drivers[o.driver].emit('isloaded');

	Match.prototype.loadDriverHooks.call(this,this.drivers[o.driver]);

	return 1;
};

Match.prototype.numberOfDrivers = function() {
	return Object.keys(this.drivers).length;
};

Match.prototype.addTeamVariant = function(teams) {
	[teams.home, teams.away] = [Match.replaceTeamName(teams.home), Match.replaceTeamName(teams.away)];
	this.teamVariants.add(JSON.stringify(teams));
	return 1;
}

Match.prototype.save = function() {
	if(!this.ng.length) return;

	this.model.set({object: { positions: this.ng.length && [...this.ng[0].positions] || null, history: this.history}});
	this.model.markModified('object');
	this.model.save(function(err) {
		if(err) {
			debug(err);
		}
	});
	//return this;
};

Match.prototype.loadDriverHooks = function(driver) {
	driver.on('event', this.handleEvent.bind(this,driver)); // on event from driver
	driver.on('remove', this.handleRemove.bind(this)); // remove instance from scraper
	driver.on('matchStatusUpdate', this.handleMatchStatusUpdate.bind(this));
	driver.on('enableScore', () => { this.matchData.scoreActive = 1; });
};

Match.prototype.handleMatchStatusUpdate = function(...updates) {
	var updated,
		updaters;

	updaters = {
		score: (newscore) => {
			let [homebefore, awaybefore] = [this.matchData.score.home, this.matchData.score.away];

			this.matchData.score.home = newscore[0] >= homebefore && newscore[0] || homebefore;
			this.matchData.score.away = newscore[1] >= awaybefore && newscore[1] || awaybefore;

			return this.matchData.score.home >= homebefore && this.matchData.score.away >= awaybefore;
		}
	};

	updated = updates.reduce((update, step) => update | (updaters[step.type] ? updaters[step.type].call(this, step.value) : false), false);

	if(updated) {
		this.matchData.lastUpdate = new Date();		
	}
};

Match.prototype.handleRemove = function(reason) {
	this.emit('remove', reason);
};

Match.prototype.handleEvent = function(driver,e) {
	//debug('Handling event');

	e.data.forEach(function(event) {
		if(typeof this.history[driver.name] === 'undefined') {
			this.history[driver.name] = {};
		}

		if(this.history[driver.name][event.unique_id] === undefined) {
			this.history[driver.name][event.unique_id] = Object.assign(event, {status: 'HANDLED'});
			this.save();

			// Remove match instance, if match ended event is handled now
			event.type === 14 
				&& Match.prototype.handleRemove.call(this, 'Match ended event was fired by driver ' + driver.name);

			// Remove match instance, if it is not football
			event.type > 100 
				&& Match.prototype.handleRemove.call(this, 'Event was not football one. Reported by ' + driver.name + '');			

			// Save data from event into matchData object
			let _seconds = event.seconds;
			event.seconds = event.seconds || this.matchData.seconds;

			// If only one driver is presented and event has null seconds property
			if(this.numberOfDrivers() == 1 && !_seconds) {
				event.seconds = Number(!!this.matchData.defaults.matchStart) 
								&& helpers.halftimeCorrection(helpers.dateDiff(new Date(this.matchData.defaults.matchStart), new Date()));
			}
			

			if(event.seconds > this.matchData.seconds) {
				this.matchData.seconds = event.seconds;
			}

			//debug('Signal for %o by driver %s with data %o', this.data.teams, driver.name, event);
		
			let params = {
				contract: 35,
				conditionalBets: []
			};

			if(this.ng.length) {

				/* Procedure may looks something like this :) */
				if(	event.type === 10 ) {

					// Apply filters - do not continue in case of match status is as follow
					if(
						helpers.isInInterval(this.matchData.seconds, [0,60*62.5]) || 
						helpers.isInInterval(this.matchData.seconds, [86*60,120*60])
					) {
						event.status = 'DENIED';						
						return;
					}	

					params.contract = 30;

					if(event.team === 'home') params.contract = Math.round(params.contract * 1.2, 0);

					if(this.matchData.scoreActive && this.lastDataUpdated(60) && helpers.isInInterval(this.matchData.seconds, [62.5*60,90*60])) {

						params.conditionalBets.push({
							preconditions: [
								this.matchData.score.home === this.matchData.score.away
							],
							conditions: [
								{matchHasFavorite: true},
								{checkScore: this.scoreToString()}
							], 
							bets: [
								{type: 'draw', side: 'BACK'},
								{type: Match.oppositeTeam(event.team), side: 'LAY'},
								{type: 'overunder'}
							]
						});

						params.conditionalBets.push({
							preconditions: [
								this.winsBy(event.team, 1),
								this.matchData.score.home > this.matchData.score.away
							],
							bets: [
								{type: 'home', side: 'BACK'},
								{type: 'overunder'}
							]
						});

						params.conditionalBets.push({
							preconditions: [
								this.winsBy(event.team, 1),
								this.matchData.score.home < this.matchData.score.away
							],
							bets: [
								{type: 'away', side: 'BACK'},
								{type: 'overunder'}
							]
						});						

					}

					this.callNg('handleSignal', event, params, 1) && debug('Tactical substitution signal has been emitted from instance %o by driver %s (%o) with data %o', this.data.teams, driver.name, this.matchData, event);
				}						

			}

		}
	}, this) 
};

// In seconds
Match.prototype.lastDataUpdated = function(seconds) {
	//console.log(helpers.dateDiff(this.matchData.lastUpdate, new Date()), "seconds");
	return helpers.dateDiff(this.matchData.lastUpdate, new Date()) < seconds;
};

Match.prototype.winsBy = function(team, ...score) {
	if(!this.matchData.scoreActive || !this.lastDataUpdated(60)) return false;

	switch(team) {
		case 'home':
			return score.filter((s) => (this.matchData.score.home - this.matchData.score.away) === s).length !== 0;
		case 'away':
			return score.filter((s) => (this.matchData.score.away - this.matchData.score.home) === s).length !== 0;
	}

	return false; 
};

Match.prototype.scoreToString = function() {
	if(!this.matchData.scoreActive || !this.lastDataUpdated(60)) return "";

	return [this.matchData.score.home, this.matchData.score.away].join(':');
};

Match.oppositeTeam = function(team) {
	return team === 'home' ? 'away' : 'home';
};

Match.prototype.callNg = function(action, ...params) {
	let result = !!1, _result = null;
	for(let instance of this.ng) {
		_result = instance[action].apply(instance, params);
		if(!_result) result = _result;
	}

	return result;
};

Match.replaceTeamName = (function() {
	var records;
	records = csvparse(fs.readFileSync(__dirname+'/../../lib/helpers/assets/teams.csv'), {delimiter: ';'});

	return function(team) {
		var filter;
		filter = records.filter((r) => r[0] === team)[0];

		return filter && filter[1] || team;
	}
})();

module.exports = Match;