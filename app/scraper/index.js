const debug = require('debug')('app::scraper::index');
const https = require('https');
const EventEmitter = require('events');
const Match = require('./model/Match');
const MatchToDriver = require('./model/MatchToDriver');
const mongoose = require('mongoose');
const drivers = require('./drivers');

// Betfair NG component
const ng = require('../ng');

const helpers = require('../lib/helpers/match.helpers');

const options = {
	db: {
		host: '127.0.0.1',
		port: '27017',
		name: 'robot'
	}
}

/** 
  * Scraper constructor
  * Scraper application is a base container of live match instances.
  *
  * It contains database handler and array of matches
**/

const Scraper = function() {
	this.matches = [];
	this.dbh = mongoose;
	this.ng = [];

	this.createDbConnection();
}

Scraper.instance = null;

// Static singleton method
Scraper.getInstance = function() {

	if(Scraper.instance instanceof Scraper) return Scraper.instance;

	Scraper.instance = new Scraper();
	return Scraper.instance; 

}

//Scraper is instance of EventEmitter and inherts its methods
Scraper.prototype = Object.create(EventEmitter.prototype);
Scraper.prototype.constructor = Scraper;

Scraper.prototype.addNgInstance = function(configObject) {
	let instance = ng.createInstance(configObject);

	if(instance instanceof ng) {
		debug('Betfair instance has been added with config: %o', configObject);
		return this.ng.push(instance);
	}
};

Scraper.prototype.performNgAction = function(action, params, callback) {
	for(let instance of this.ng) {
		callback(instance[action].apply(instance, params));
	}
};

// After connecting to database, procedure of run polling is performed
Scraper.prototype.run = function() {
	this.checkForUpdates();
	setInterval(
		this.checkForUpdates.bind(this), 
		30000
	);
}

Scraper.prototype.createDbConnection = function() {
	this.connected = !!0;

	this.dbh.connect(
		'mongodb://' + 
			Scraper.options.db.host + ':' +
			Scraper.options.db.port + '/' +
			Scraper.options.db.name
	)

	this.dbh.connection
		.once('open', function() {
			debug('Connected to database - success.');
			this.connected = !this.connected; 
			this.run();
		}.bind(this))
		.on('error', function(error) {
			debug('Error while connecting to database! %s', error);
			throw new Error(error);
		});
}

Scraper.prototype.checkForUpdates = function() {
	//debug('Checking for presence of new matches returned by crawlers');
	MatchToDriver.model.find({},function(err,m) {
		//debug('Found %d rows', m.length);

		// For each matchtodriver perform search if the one is already present
		if(m.length == 0) {
			return;
		}

		m.filter(function(match) {
			return match.teams.home !== undefined && match.teams.away !== undefined;
		})

		m.forEach(function(match) {
			// Return if match is found in active instances
			let instance = this.checkMatchArray(match);
			//debug('Processing match: %o', match);
			if(!instance) {
				this.addMatch(
					new Match(match)
				);
			} else {
				instance.drivers[match.driver] === undefined &&
					instance.addDriver({
						driver: match.driver,
						internal_id: match.internal_id
					}) && instance.addTeamVariant(match.teams) &&
					debug('Driver %s has beed added into instance with variant %s. Actual status: %o (%s)', match.driver, Array(match.teams.home, match.teams.away), instance.data.teams, Object.keys(instance.drivers));

				if(!instance.foundOnBF) this.assignNg(instance);
			}

			match.remove(function(err) {
			});

		}.bind(this));

		// Show all specific driver events
		//debug(drivers.sazka.allevents);
		//debug(drivers.tipsport.allevents);
		//debug(drivers.unibet.allevents);
		//debug(drivers.marathonbet.allevents);
		//debug(drivers.skybet.allevents);

	}.bind(this));

	// Check for instances that are active too long (120 min+)
	this.checkForLapsedMatches();

	// Remove frozen matches
	this.checkAndDeleteFrozenMatches();

}

Scraper.prototype.checkForLapsedMatches = function(mins = 120) {
	var self = this;

	this.matches.filter(function(m) {
		if(( m.data.created < (parseInt(Date.now()/1000) - mins*60) )) {
			self.removeMatch(m, 'Match was active more than ' + mins + ' minutes');
		}
	});
};

Scraper.prototype.checkAndDeleteFrozenMatches = function(mins = 60) {
	var self = this;

	this.matches.filter(function(m) {
		if(( m.frozen && m.frozen < (parseInt(Date.now()/1000) - mins*60) )) {
			let index = self.matches.indexOf(m);
			self.matches.splice(index,1);
			debug('Frozen match lapsed - remove it');
		}
	});

}

// This function checks the presence for match instance. 
// Return that instance, if found.
Scraper.prototype.checkMatchArray = function(match) {
	// For each instance in this scraper instance
	let found = 0;
	this.matches.filter(function(m) {
		if(found) return;
		found = this.checkTeamsEqual(
				m.data.teams, [match.teams.home, match.teams.away]
		) && m;
	}.bind(this));

	//debug("Founded in instances? %s",found?'YES':'NO');
	//if(found) debug("%o",match);

	return found;
}

// Check if teams in both arrays a and be are the same
Scraper.prototype.checkTeamsEqual = function(a,b) {
	return helpers.checkForPairCandidate(a[0],b[0],true) +
		helpers.checkForPairCandidate(a[1],b[1],true) > 1;
}

Scraper.prototype.addMatch = function(o) {
	if(typeof o && o instanceof Match) {
		this.matches.push(o);
		this.addHooks(o);
		this.assignNg(o);

		return 1;	
	}

	return 0;
}

Scraper.prototype.removeMatch = function(o, reason = undefined) {
	if(o.frozen) return;

	// Remove all listeners from drivers
	Object.keys(o.drivers).filter(function(driver) {
		o.drivers[driver].removeAllListeners();
		o.drivers[driver].destroy();
	});

	// Remove all listeners and automation from ng, if presented
	if(o.ng.length) {
		o.callNg('unmount');
	}

	// Remove all listeners from match
	o.removeAllListeners();

	// Freeze match instance
	o.frozen = o.frozen === undefined && parseInt(Date.now() / 1000);
	debug('Freeze match instance: %o due the "%s"', o.data, reason);	

	//debug('Number of instances after: %d', this.matches.length);

	return 1;
}

Scraper.prototype.addHooks = function(o) {
	var self = this;
	o.on('remove', function(reason) {
		self.removeMatch(o, reason);
	});

	return 1;
}

Scraper.prototype.assignNg = function(o) {
	var self = this;
	this.performNgAction('assignToInstance', [o], function(result) {

		result.then(
			function(bettor) {
				debug('Bettor mounted into %o (%s)', o.data.teams, Object.keys(o.drivers));
				o.foundOnBF = 1;
				o.ng.push(bettor);
			}
		).catch(function(e) {
			//console.log(e);
			//o.ng = null;
		});

	});
}

Scraper.options = options;

module.exports = { 
	scraper: Scraper,
	options: options
}