const debug = require('debug')('app::scraper::drivers::skybet');
const baseDriver = require('./base');
const matchRecursive = require('../../lib/helpers/matchRecursive');
const request = require('request');

const eventTypes = {
	'Injury Break': baseDriver.eventTypes.INJURY, //OK
	'Substitution': baseDriver.eventTypes.SUBSTITUTION, //OK
	'Shot Off Target': baseDriver.eventTypes.SHOT_OFF, // ok
	'Goal Kick': baseDriver.eventTypes.GOAL_KICK, //ok
	'Throw': baseDriver.eventTypes.THROW_IN,//ok
	'Offside': baseDriver.eventTypes.OFFSIDE, // ok
	'Free Kick': baseDriver.eventTypes.FREE_KICK, //ok
	'Shot On Target': baseDriver.eventTypes.SHOT_ON, //ok
	'Goal': baseDriver.eventTypes.GOAL, //ok
	'Corner': baseDriver.eventTypes.CORNER_KICK
};

var options = {
	url: 'https://1.push.skybet.com:8443/socket.io/1/?t='+Date.now(),
	headers: {
		'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
		'Connection': 'keep-alive',
		'Origin': 'https://m.skybet.com',
		'Accept': '*/*',
		'Accept-Encoding': 'gzip, deflate, br'
	}
};

var options2 = {
	url: 'https://m.skybet.com/',
	jar: true,
	headers: {
		'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
	},
	proxy: 'http://35.178.73.158:9999'
};

String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  if (this.length === 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return Math.abs(hash);
};


function driverSkybet(data) {

	this.startMagic().then(() => {

		this.options = {
			host: 'wss://1.push.skybet.com:8443/socket.io/1/websocket/'+this.socketsecret,
			packet: '5:::{"name":"subscribe","args":[{"keys":["sc.'+data.internal_id+'"],"url":"/football/football-live/event/'+data.internal_id+'","useragent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36","session":"'+this.cookiesecret+'","resetPrevious":true}]}',
			resend: false
		}

		this.name = "Skybet";
		this.type = baseDriver.driverTypes['WS'];

		baseDriver.call(this,data);

		this.emit('isloaded');
		this.emit('enableScore');

	});
};

driverSkybet.allevents = {};

driverSkybet.prototype = Object.create(baseDriver.prototype);
driverSkybet.prototype.constructor = driverSkybet;

driverSkybet.prototype.processData = function(obj, data) {
	var json = data, x;

	[...x] = json.cm.match(/(?=(?:[\w\s]+:\s+)[\w\s]+)([\w\s]+):\s+([\w\s]+)|(?!(?:[\w\s]+:\s+)[\w\s]+)([\w\s]+)/) || [];

	let score = [json.gl.hm,json.gl.aw];

	this.emit('matchStatusUpdate', {type: 'score', value: score});

	// TRU: Attacking
	// [ 'TRU: Attacking', 'TRU', 'Attacking', undefined ]
	// Injury Break
	// [ 'Injury Break', undefined, undefined, 'Injury Break' ]

	let [type,team] = [ x[2]?x[2]:x[3], x[1]?x[1]:null ]

	let eventObject = {
		'unique_id': type.hashCode() + json.se,
		'date': new Date(),
		'type': driverSkybet['eventTypes'][type],
		'name': json.cm,
		'team': team, 
		'time': String(Math.floor(json.se/60))+":"+(String(json.se%60).length == 1 ? String('0' + json.se%60) : String(json.se%60)),
		'seconds': json.se,
		'other': {}	
	}


	if(driverSkybet.allevents[String(type)]) driverSkybet.allevents[String(type)] += 1;
	else driverSkybet.allevents[String(type)] = 1;

	//console.log(driverUnibet.allevents);

	//console.log(eventObject);

	eventObject.type !== undefined && 
		obj.events.push(eventObject);
}

driverSkybet.prototype.decoder = function(str) {

	str = str.replace(/^[\d]:{2,3}/,'');

	var json = null, data = null;

	if(str) {
		try {
			json = JSON.parse(str);
		} catch(e) {
			debug("Error while parsing JSON: %s", str);
		}
	}

	if(json) {
		data = json.args[0].pl;
		if(!data.cm) return null;

		return data;	
	}

	return null;
}

driverSkybet.prototype.startMagic = async function() {
	this.socketsecret = await this.sniffSocketSecret();
	this.cookiesecret = await driverSkybet.sniffCookieSecret;
};

driverSkybet.sniffCookieSecret = (function() {
	return new Promise(function(resolve,reject) {
		request(options2, function(err,res,body) {
			try {
				resolve(res.headers['set-cookie'].join("").match(/TINYSESSID=([\w]+);/)[1]);
			} catch(e) {
				debug("No cookie secret found.");
				//reject(e);
			}
		});
	});
})();

driverSkybet.prototype.sniffSocketSecret = function() {
	return new Promise(function(resolve,reject) {
		request(options, function(err,res, body) {
			if(!err) {
				resolve(body.split(':')[0]);
			} else reject(new Error(err));
		});
	});
};

driverSkybet.eventTypes = eventTypes;

module.exports = driverSkybet;