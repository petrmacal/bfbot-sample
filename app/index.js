const debug = require('debug')('app::index');
const scraper = require('./scraper').scraper;

debug("Starting application....");

var data 	= '', 
	app 	= scraper.getInstance();

/* Default instance */
app.addNgInstance({
	bettorConfig: {
		maxStake: 15
	}
});

/* 
 *	More instances may be added calling the addNgInstance method, for example.:

	app.addNgInstance({
		applicationKey: 'APIKEY', 
		username: 'username',
		password: 'passord',
		certFile: '../assets/cert/cert.crt',
		keyFile: '../asets/cert/cert.key',
		// Congiguration object
		bettorConfig: {
			maxStake: 5
		}
	});
*/