/**
  * Various helpers for Match model
*/

const helpers = {
	checkForPairCandidate: function(a,b,returnValue=false,c = 0.8) {
		// Helper functions
		var checkForIdentical = function(a,b) {
			return a.toLowerCase() === b.toLowerCase() ? 1 : null;	
		}

		var calculateIfOneIsEmpty = function(a,b) {
			return a === undefined || a.length === 0 || b === undefined || b.length === 0 ? 1 : null;
		}

		// Calculate result if one string is empty
		if(result = calculateIfOneIsEmpty(a,b) !== null) {
			return returnValue && 0 || !!0;
		}

		// Calculate result if identical
		if((result = checkForIdentical(a,b)) !== null) {
			return returnValue && 1 || !!1;
		}

		var abbrs = require('./assets/abbrs'),
			diacritics = require('diacritics').remove;
			normalize =  require('normalize-for-search'), 
			_virtualMap = {};
		
		// Map every abbrs into virtal map of every possible substitution
		Object.keys(abbrs).forEach(function(key) {
			if(typeof abbrs[key] === 'object') {
				for(let r = 0; r < abbrs[key].length; r++) {
					_virtualMap[abbrs[key][r]] = key;
				}
			} else {
				_virtualMap[key] = abbrs[key];
			}
		});

		// Convert to array of words
		[a,b] = [normalize(diacritics(a)).split(' '),normalize(diacritics(b)).split(' ')];

		// Count the coeficient of similarity
		var union = a.length + b.length,
			intersection = 0;

		a.forEach(function(string1) {
			for(let i = 0; i <= b.length; i++) {
				if(string1 == b[i]) {
					intersection += 1;
					b.splice(i,1);
				} else {
					// If first string from virtualmap is the same as second or viceversa
					if(b[i] !== undefined && 
						(
							_virtualMap[string1] === b[i] || 
							string1 === _virtualMap[b[i]])
					) {
						intersection += 0.8;
						b.splice(i,1);						
					}
				}
			}
		});

		return returnValue && 2*intersection/union || 2*intersection/union >= c;

		// TODO: heuristic test for letters
	},

	isInInterval: function(number,interval) {
		return number >= interval[0] && number <= interval[1];
	},

	dateDiff: function(date1, date2) {
		return Math.round(Math.abs(date1.getTime() - date2.getTime()) / 1000);
	},

	// Halftime correction
	halftimeCorrection: function(seconds) {
		return (seconds > (2700 + 900) && seconds -1080 || seconds); 
	}
};

module.exports = helpers;