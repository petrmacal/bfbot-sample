module.exports = {
  apps : [

    // Robot application
    {
      name      : 'Robot',
      script    : 'index.js',
      env: {
        DEBUG: '*',
        DEBUG_COLORS: true,
        NODE_ENV: 'development'
      },
      env_production : {
        NODE_ENV: 'production'
      }
    },

    // Database interface
    {
      name	: 'Database',
      script    : 'npm',
      args	: 'start',
      cwd	: '/home/ec2-user/Projects/adminMongo/'
    },

    // Terminal
    {
      name      : 'Web terminal',
      script    : 'terminal.js',
      cwd : '/home/ec2-user/Projects/robot/',
      env: {
        NODE_ENV: 'development'
      },
      env_production : {
        NODE_ENV: 'production'
      }
    }   
   
  ]
};
