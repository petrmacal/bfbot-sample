var debug = require('debug')('process:getSkybetIds');
var mongoose = require('mongoose');
var config = require('../../app/scraper').options;
var request = require('request');
var cheerio = require('cheerio');

var MatchToDriver = require('../../app/scraper/model/MatchToDriver');

// Define main Skybet live endpoint and options
const endpoint = "https://m.skybet.com/in-play";
const options = {
	url: endpoint,
	jar: true,
	headers: {
		'Accept': 'application/hijax+json'
	},
	proxy: 'http://35.178.73.158:9999'
};

mongoose.connect(
	'mongodb://' + 
		config.db.host + ':' +
		config.db.port + '/' +
		config.db.name
);

var Match = MatchToDriver.model;

var beforeProcess = function beforeProcess() {
	return new Promise(function(resolve,reject) {
		Match.remove({'driver':'skybet'}, function(err) {
			if(err) reject(err);
			resolve("removed");
		});
	});
}

var stripAbbrs = function stripAbbrs(str) {
	return str.replace(/(.*)-[\w]{2}/,'$1');
}

var processData = function processData(data) {
	debug("Processing data.. %d", data.length);
	let processed = data.length;

	return new Promise(function(resolve,reject) {
		for(let i = 0; i < data.length; i++) {
			// Declare all necceasry variables
			let md = data[i];

			var match = new Match({
				driver: 'skybet',
				internal_id: md.id,
				teams: {
					home: stripAbbrs(md.homeName),
					away: stripAbbrs(md.awayName)
				},
				unique_id: 'skybet' + md.id,
				date: new Date()
			});

			match.save(function(err,match) {
				processed--;
				if(err) return;
				
				debug('Match has been saved to DB. Info: %o', match);

				(processed == 0) && resolve('done');
			});
		}	
	});	
}


beforeProcess().then(function() {

	request(options,function(err, res, body) {
		if(err) {
			debug(err);
			return;
		}

		let events = [];

		let json, $;
		try {
			json = JSON.parse(body);
			$ = cheerio.load(json.body);

			$('td.cell--link a.cell--link__link').filter(function(i,elem) {
				return $(this).attr('href').match(/football-live/);
			}).each(function(i,elem) {
				let [team1,team2] = [$(elem).find('.live-team-name').get(0), $(elem).find('.live-team-name').get(1)];
				let id = $(elem).attr('href').match(/event\/([0-9]+)/)[1];

				events.push({
					homeName: $(team1).text().trim(),
					awayName: $(team2).text().trim(),
					id: id
				});
			});

			processData(events).then(function() { mongoose.connection.close(); });
		} catch(e) {
			debug(e);
		}

	});

}).catch(function(e) {
	debug('Error: %o', e);
});
